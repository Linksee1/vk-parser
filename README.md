# Тестовое задание #

### Сборка проекта ###

gulp build - собрать статику.
gulp - развернуть browserSync, слушать изменения и запускать пересборку.

### Использовалсь следующие инструменты ###

* Gulp
* Jade
* SCSS
* ES2015(Babel)

* Проведена конкатенация, минификация и обфускация кода
* Минимизированы и конкатенированы таблицы стилей
* Минимизирован html

### Фреймфорк ###

* AngularJS

### Плагины ###

* Owl Carousel
