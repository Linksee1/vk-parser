function newsSingleController($scope, $window, $rootScope, $routeParams) {
  console.log($routeParams.newsId);
  $scope.newsNumber = $routeParams.newsId;
  $scope.newsToShow = $rootScope.newsArray[$scope.newsNumber];
  console.log($scope.newsToShow);
  $rootScope.pageTitle = `Новость ${$scope.newsNumber}`;

  setTimeout(function(){
    $("img[src='']").parents('.item').remove();
    $(".owl-example").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      items : 1,
      itemsDesktop : false,
      itemsDesktopSmall : false,
      itemsTablet: false,
      itemsMobile : false
    });
  }, 100);
  $scope.getBack = function () {
    $window.location.href = "/#/search";
  };
  
}

angular.module('PortfolioApp').component('newsSingle', {
  templateUrl: '../main/news-single/news-single.template.html',
  controller: newsSingleController
});