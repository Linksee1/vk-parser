function navController($http, $scope, $rootScope, $window) {
  $rootScope.pageTitle = "Лента новостей";
  function callVK(string) {
    $http.jsonp(`https://api.vk.com/method/newsfeed.search?q=${string}&extended=1&callback=JSON_CALLBACK`)
      .success(function (result) {
        $scope.availableData = result.response;
        $scope.submitStatus = 1;
        $rootScope.newsArray = $scope.availableData;
        console.log($scope.availableData);
      }).then(function () {
      setTimeout(function(){
        $("img[src='']").parents('.item').remove();
        $(".owl-example").owlCarousel({
          navigation : true,
          slideSpeed : 300,
          paginationSpeed : 400,
          items : 1,
          itemsDesktop : false,
          itemsDesktopSmall : false,
          itemsTablet: false,
          itemsMobile : false
        });
      }, 1000);
      $scope.answer = new FormData();
      $scope.answer.append("query", string);
      $http({
        "method": "post",
        "data": $scope.answer || "not empty",
        "headers": {
          "accept": "application/json",
          'Content-Type': undefined
        },
        "url": "http://ft.dev.hismith.ru/stat/create/"
      }).then(function(result) {
        console.log(result);
      });
    });
  }
  $scope.submitStatus = 0;
  $scope.formSubmit = function () {
    $scope.submitStatus = 2;
    callVK($scope.name);
  };
  if ($rootScope.variable)
  {
    callVK($rootScope.variable);
    $scope.name = $rootScope.variable;
  }
  $scope.showNews = function (number) {
    $window.location.href = `/#/news/${number}`;
  };
  $scope.availableData = $rootScope.newsArray;
  $scope.toPopUp = function (url) {
    $scope.hide = false;
    $scope.popUpUrl = url;
  };
  $scope.hide = true;
  $scope.getBack = function () {
    $window.location.href = "/#/main";
  };
}
angular.module('PortfolioApp').component('navigation', {
  templateUrl: '../main/navigation/navigation.template.html',
  controller: navController
});