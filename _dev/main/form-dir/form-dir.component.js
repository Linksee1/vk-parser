function formController($rootScope, $scope, $window, $http) {
  $rootScope.pageTitle = "Поиск";
  $scope.formSubmit = function () {
    $rootScope.variable = $scope.name;
    $window.location.href = "/#/search";
    console.log( $rootScope.variable);
    $scope.answer = new FormData();
    $scope.answer.append("query", $rootScope.variable);
    $http({
      "method": "post",
      "data": $scope.answer || "not empty",
      "headers": {
        "accept": "application/json",
        'Content-Type': undefined
      },
      "url": "http://ft.dev.hismith.ru/stat/create/"
    }).then(function(result) {
      console.log(result);
    });
  };
}
  
angular.module('PortfolioApp').component('formDir', {
  templateUrl: '../main/form-dir/form-dir.template.html',
  controller: formController
});