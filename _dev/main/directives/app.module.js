'use strict';
angular.module('PortfolioApp', [
  'ngRoute',
  'ngSanitize'
]).controller('mainCtrl', ["$rootScope", function($rootScope) {
  $rootScope.pageTitle = "Поиск";

}]);
angular.module('PortfolioApp').config(["$routeProvider", function ($routeProvider) {
  $routeProvider.when("/search", {
    template: '<navigation></navigation>'
  })
    .when("/main", {
    template: '<form-dir></form-dir>'
  })
    .when("/news/:newsId", {
    template: '<news-single></news-single>'
  })
    .otherwise({redirectTo: '/main'});
}]);